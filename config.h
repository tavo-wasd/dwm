/* See LICENSE file for copyright and license details. */
#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 10;        /* gaps between windows */
static const unsigned int snap      = 5;       /* snap pixel */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 0;        /* 0 means bottom bar */
static const char *fonts[]          = { "JetBrainsMono:bold:size=10" };
static const char dmenufont[]       = "JetBrainsMono:bold:size=10";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
/* custom colors */
static const char col_light[]       = "#fabd2f";
static const char col_dark[]        = "#1d2021";
static const char col_black[]       = "#000000";
static const char *colors[][3]      = {
/* --------------- */
	/*               fg         bg         border   */
	[SchemeNorm] = { col_dark, col_black, col_dark },
	[SchemeSel] =  { col_dark, col_light, col_light },
};

/* tagging */
static const char *tags[] = { " 1 ", " 2 ", " 3 ", " 4 ", " 5 " };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class             instance  title           tags mask  isfloating  isterminal  noswallow  monitor */
	{ "st",              NULL,     NULL,           0,         0,          1,           0,        -1 },
	{ "Display-im6.q16", NULL,     NULL,           0,         1,          1,           0,        -1 },
	{ "zbar",            NULL,     NULL,           0,         1,          1,           0,        -1 },
	{ "qemu-system-x86_64", NULL,     NULL,           0,         1,          1,           0,        -1 },
	{ NULL,              NULL,     "Event Tester", 0,         0,          0,           1,        -1 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "",      tile },    /* first entry is default */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

#define STATUSBAR "dwmblocks"

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "st", NULL };

/* custom commands */
static const char *menucmd[]     = { "menu", "Run:", "run", NULL };
static const char *filemanager[] = { "thunar", NULL };
static const char *dbookmarks[]  = { "menu-bookmarks", NULL };
static const char *dpass[]       = { "dwmpass", NULL };
static const char *dpassotp[]    = { "dwmotp", NULL };
static const char *klayout[]     = { "setxkbmap-next", NULL };
static const char *doutput[]     = { "menu-output", NULL };
static const char *dinput[]      = { "menu-input", NULL };
static const char *dpower[]      = { "menu-power", NULL };
static const char *browser[]     = { "firefox-hardened", NULL };
static const char *brightdown[]  = { "brightdown", NULL };
static const char *brightup[]    = { "brightup", NULL };
static const char *clipmenu[]    = { "clipmenu", NULL };
static const char *voldown[]     = { "voldown", NULL };
static const char *volup[]       = { "volup", NULL };
static const char *micdown[]     = { "micdown", NULL };
static const char *micup[]       = { "micup", NULL };
static const char *prtsc[]       = { "prtsc", NULL };
static const char *mute[]        = { "mute", NULL };
static const char *micmute[]     = { "micmute", NULL };
/* --------------- */

#include "movestack.c"
static const Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_r,      spawn,          {.v = menucmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY|ShiftMask,             XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_c,      killclient,     {0} },
	{ MODKEY|ShiftMask,             XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ShiftMask,             XK_m,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_t,      togglefloating, {0} },
	{ MODKEY,                       XK_s,      togglesticky,   {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -5 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +5 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
    /* custom binds */
	{ MODKEY,                       XK_f,      spawn,          {.v = filemanager } },
	{ MODKEY,                       XK_b,      spawn,          {.v = dbookmarks } },
	{ MODKEY|ShiftMask,             XK_o,      spawn,          {.v = dpassotp } },
	{ MODKEY|ShiftMask,             XK_c,      spawn,          {.v = clipmenu } },
	{ MODKEY,                       XK_w,      spawn,          {.v = browser } },
	{ MODKEY,                       XK_o,      spawn,          {.v = doutput } },
	{ MODKEY,                       XK_space,  spawn,          {.v = klayout } },
	{ MODKEY,                       XK_p,      spawn,          {.v = dpower } },
	{ MODKEY,                       XK_i,      spawn,          {.v = dinput } },
	{ MODKEY|ShiftMask,             XK_p,      spawn,          {.v = dpass } },
	{ 0,             XF86XK_MonBrightnessDown, spawn,          {.v = brightdown } },
	{ 0,             XF86XK_MonBrightnessUp,   spawn,          {.v = brightup } },
	{ 0,             XF86XK_AudioLowerVolume,  spawn,          {.v = voldown } },
	{ 0,             XF86XK_AudioRaiseVolume,  spawn,          {.v = volup } },
	{ 0,             XF86XK_AudioMute,         spawn,          {.v = mute } },
	{ ShiftMask,     XF86XK_AudioLowerVolume,  spawn,          {.v = micdown } },
	{ ShiftMask,     XF86XK_AudioRaiseVolume,  spawn,          {.v = micup } },
	{ ShiftMask,     XF86XK_AudioMute,         spawn,          {.v = micmute } },
	{ 0,             XF86XK_AudioMicMute,      spawn,          {.v = micmute } },
	{ 0,                            XK_Print,  spawn,          {.v = prtsc } },
    /* ------------ */
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

